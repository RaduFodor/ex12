package ro.orange;

public class GenTypes<T1, T2> {
    public T1 name;
    public T2 value;

    public GenTypes(T1 name, T2 value) {        // constructor with input parameters
        this.name = name;
        this.value = value;
    }

    public T1 getName() {
        return name;
    }

    public T2 getValue() {
        return value;
    }
}
